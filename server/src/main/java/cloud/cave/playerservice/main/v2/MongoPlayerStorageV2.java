package cloud.cave.playerservice.main.v2;

import cloud.cave.playerservice.main.ConnectionConfig;
import com.google.gson.Gson;
import com.mongodb.MongoClientSettings;
import com.mongodb.ServerAddress;
import com.mongodb.client.*;
import com.mongodb.client.model.ReplaceOptions;
import com.mongodb.connection.ClusterConnectionMode;
import com.mongodb.connection.ClusterType;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Filters.eq;

public class MongoPlayerStorageV2 implements PlayerStorageV2 {
    private static final Gson gson = new Gson();

    private final static String playerRecord = "PlayerRecord";
    private final static String playerservice = "playerservice";
    private final MongoClient mongoClient;
    private final MongoCollection<Document> playerCollection;

    public MongoPlayerStorageV2(ConnectionConfig serverConfig) {
        ServerAddress dbShards = new ServerAddress(serverConfig.getHostName(), serverConfig.getPortNumber());
        mongoClient = MongoClients.create(
                MongoClientSettings.builder()
                                   .applyToSocketSettings(builder -> builder.connectTimeout(5, TimeUnit.SECONDS))
                                   .applyToClusterSettings(builder -> builder.hosts(Collections.singletonList(dbShards)))
                                   .build());

        MongoDatabase database = mongoClient.getDatabase(playerservice);
        playerCollection = database.getCollection(playerRecord);
    }

    public MongoPlayerStorageV2(String replicaSet, List<ConnectionConfig> serverConfigs) {
        List<ServerAddress> dbShards = serverConfigs.stream().map(config -> new ServerAddress(config.getHostName(), config.getPortNumber()))
                .collect(Collectors.toList());

        mongoClient = MongoClients.create(
                MongoClientSettings.builder()
                                   .applyToSocketSettings(builder -> builder.connectTimeout(5, TimeUnit.SECONDS))
                                   .applyToClusterSettings(builder -> builder.hosts(dbShards)
                                                                             .requiredReplicaSetName(replicaSet)
                                                                             .requiredClusterType(ClusterType.REPLICA_SET)
                                                                             .mode(ClusterConnectionMode.MULTIPLE))
                                   .build());

        MongoDatabase database = mongoClient.getDatabase(playerservice);
        playerCollection = database.getCollection(playerRecord);
    }

    @Override
    public PlayerRecordV2 getPlayerByID(String playerID) {
        final Document document = playerCollection.find(eq("playerID", playerID))
                                                  .iterator().tryNext();
        if(document == null) {
            return null;
        }
        return gson.fromJson(document.toJson(), PlayerRecordV2.class);
    }

    @Override
    public UpdateStatus updatePlayerRecord(PlayerRecordV2 record) {
        final String playerJson = gson.toJson(record);
        final Document playerDocument = Document.parse(playerJson);
        final PlayerRecordV2 existingPlayer = getPlayerByID(record.getPlayerID());
        playerCollection.replaceOne(eq("playerID", record.getPlayerID()),
                                    playerDocument, new ReplaceOptions().upsert(true));
        return existingPlayer == null ? UpdateStatus.CREATED
                                      : UpdateStatus.UPDATED;
    }

    @Override
    public List<PlayerRecordV2> computeListOfPlayersAt(String positionString) {
        List<PlayerRecordV2> result = new ArrayList<>();
        final MongoCursor<Document> playerDocuments = playerCollection.find(eq("positionAsString", positionString)).cursor();
        while(playerDocuments.hasNext()) {
            Document next = playerDocuments.next();
            result.add(gson.fromJson(next.toJson(), PlayerRecordV2.class));
        }
        return result;
    }

    @Override
    public void disconnect() {
        mongoClient.close();
    }

    @Override
    public String toString() {
        return "{ type : "+ MongoPlayerStorageV2.class.getSimpleName()+", serverDescriptions : [" +
                mongoClient.getClusterDescription()
                           .getServerDescriptions().stream()
                           .map(sd -> "\""+sd.toString()+"\"")
                           .collect(Collectors.joining(", ")) + "]" +
                "}";
    }
}
