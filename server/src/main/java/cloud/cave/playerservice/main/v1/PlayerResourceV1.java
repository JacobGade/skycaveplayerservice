package cloud.cave.playerservice.main.v1;

import cloud.cave.playerservice.main.Translator;
import cloud.cave.playerservice.main.v2.PlayerRecordV2;
import cloud.cave.playerservice.main.v2.PlayerStorageV2;
import cloud.cave.playerservice.main.v2.UpdateStatus;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.RouteGroup;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static cloud.cave.playerservice.main.StringUtils.isNullOrEmpty;
import static spark.Spark.get;
import static spark.Spark.put;

public class PlayerResourceV1 implements RouteGroup {
  private final static Logger logger = LoggerFactory.getLogger(PlayerResourceV1.class);
  private final static Gson gson = new Gson();

  private final static Pattern positionRegex = Pattern.compile("\\(-?\\d+,-?\\d+,-?\\d+\\)");
  private final static String jsonContentType = "application/json";

  private final PlayerStorageV2 playerStorage;
  private final Translator<PlayerRecordV1, PlayerRecordV2> translator;

  public PlayerResourceV1(PlayerStorageV2 playerStorage,
                          Translator<PlayerRecordV1, PlayerRecordV2> translator) {
    this.playerStorage = playerStorage;
    this.translator = translator;
  }

  @Override
  public void addRoutes() {
    get("", this::getPlayersAtPosition, this::jsonTransformer);
    get("/:id", this::getPlayer, this::jsonTransformer);
    put("/:id", this::updatePlayer);
  }

  private String jsonTransformer(Object o) {
    if(o == null) {
      return "";
    }
    return gson.toJson(o);
  }

  private List<PlayerRecordV1> getPlayersAtPosition(Request request, Response response) {
    final String position = request.queryParams("position");
    if(!positionRegex.matcher(position).matches()){
      response.status(400);
      return null;
    }
    response.type(jsonContentType);
    return playerStorage.computeListOfPlayersAt(position).stream()
                        .map(translator::back)
                        .collect(Collectors.toList());
  }

  private String updatePlayer(Request request, Response response) {
    final String id = request.params(":id");
    response.type("nothing");
    final PlayerRecordV1 playerRecord;
    try {
      playerRecord = gson.fromJson(request.body(), PlayerRecordV1.class);
    } catch (JsonSyntaxException e) {
      response.status(422);
      return "";
    }
    if(!isValidateRecord(playerRecord, id)){
      response.status(422);
      return "";
    }
    playerRecord.setId(id);
    final UpdateStatus updateStatus = playerStorage.updatePlayerRecord(translator.forward(playerRecord));
    switch (updateStatus) {
      case CREATED:
        response.header("Location", request.url());
        response.status(201);
        break;
      case UPDATED:
        response.status(204);
        break;
    }
    return "";
  }

  private boolean isValidateRecord(PlayerRecordV1 playerRecord, String id) {
    return id.equals(playerRecord.getPlayerID())
            && !isNullOrEmpty(playerRecord.getGroupName())
            && !isNullOrEmpty(playerRecord.getPlayerName())
            && !isNullOrEmpty(playerRecord.getPositionAsString())
            && positionRegex.matcher(playerRecord.getPositionAsString()).matches()
            && !isNullOrEmpty(playerRecord.getRegion());
  }

  private PlayerRecordV1 getPlayer(Request request, Response response) {
    String params = request.params(":id");

    final PlayerRecordV2 playerByIDV2 = playerStorage.getPlayerByID(params);
    final PlayerRecordV1 playerByIDV1 = translator.back(playerByIDV2);

    if(playerByIDV1 == null){
      response.status(404);
      response.type("nothing");
      return null;
    }
    response.type(jsonContentType);
    return playerByIDV1;
  }
}
