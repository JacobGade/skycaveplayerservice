package cloud.cave.playerservice.main.v2;

import cloud.cave.playerservice.main.Region;

public class PlayerRecordV2 {
  private String playerID;
  private String firstName;
  private String lastName;
  private String groupName;
  private Region region;

  private String positionAsString;
  private String sessionID;

  public PlayerRecordV2(String playerID,
                        String firstName,
                        String lastName,
                        String groupName,
                        Region region,
                        String positionAsString,
                        String sessionID) {
    this.playerID = playerID;
    this.firstName = firstName;
    this.lastName = lastName;
    this.groupName = groupName;
    this.region = region;
    this.positionAsString = positionAsString;
    this.sessionID = sessionID;
  }

  public String getPlayerID() {
    return playerID;
  }
  public String getGroupName() {
    return groupName;
  }
  public String getPositionAsString() {
    return positionAsString;
  }
  public boolean isInCave() {
    return sessionID != null;
  }
  public Region getRegion() {
    return region;
  }
  /**
   * get the session ID; if it is null
   * then the player is not presently in the cave
   * @return the session ID or null in case
   * no session exists for the given player
   */
  public String getSessionId() {
    return sessionID;
  }

  public void setPositionAsString(String positionAsString) {
    this.positionAsString = positionAsString;
  }
  public void setSessionId(String sessionId) {
    this.sessionID = sessionId;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((playerID == null) ? 0 : playerID.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    PlayerRecordV2 other = (PlayerRecordV2) obj;
    if (playerID == null) {
      if (other.playerID != null)
        return false;
    } else if (!playerID.equals(other.playerID))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "PlayerRecord [playerID=" + playerID + ", firstName=" + firstName
            + ", lastName=" + lastName
            + ", groupName=" + groupName + ", region=" + region
            + ", position=" + positionAsString + ", sessionID=" + sessionID
            + "]";
  }

  public void setId(String id) {
    playerID = id;
  }

  public String getFirstName() {
    return firstName;
  }

  public String getLastName() {
    return lastName;
  }
}
