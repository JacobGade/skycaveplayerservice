package cloud.cave.playerservice.main;

public interface Translator<T, T1> {
    T1 forward(T input);
    T back(T1 input);
}
