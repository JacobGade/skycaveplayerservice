package cloud.cave.playerservice.main;

public enum Region {
  UNKNOWN, AARHUS, COPENHAGEN, ODENSE, AALBORG
}
