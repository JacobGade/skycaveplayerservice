package cloud.cave.playerservice.main.v1;

import cloud.cave.playerservice.main.Translator;
import cloud.cave.playerservice.main.v2.PlayerRecordV2;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.RouteGroup;

import java.util.List;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static cloud.cave.playerservice.main.StringUtils.isNullOrEmpty;
import static spark.Spark.get;
import static spark.Spark.put;

public class PlayerResourceV2Temp implements RouteGroup {
  private final static Logger logger = LoggerFactory.getLogger(PlayerResourceV2Temp.class);
  private final static Gson gson = new Gson();

  private final static Pattern positionRegex = Pattern.compile("\\(-?\\d+,-?\\d+,-?\\d+\\)");
  private final static String jsonContentType = "application/json";

  private final PlayerStorageV1 playerStorage;
  private final Translator<PlayerRecordV2, PlayerRecordV1> translator;

  public PlayerResourceV2Temp(PlayerStorageV1 playerStorage) {
    this.playerStorage = playerStorage;
    this.translator = new PlayerTranslatorV2ToV1();
  }

  @Override
  public void addRoutes() {
    get("", this::getPlayersAtPosition, this::jsonTransformer);
    get("/:id", this::getPlayer, this::jsonTransformer);
    put("/:id", this::updatePlayer);
  }

  private String jsonTransformer(Object o) {
    if(o == null) {
      return "";
    }
    return gson.toJson(o);
  }

  private List<PlayerRecordV2> getPlayersAtPosition(Request request, Response response) {
    final String position = request.queryParams("position");
    if(!positionRegex.matcher(position).matches()){
      response.status(400);
      return null;
    }
    response.type(jsonContentType);
    return playerStorage.computeListOfPlayersAt(position).stream()
                        .map(translator::back)
                        .collect(Collectors.toList());
  }

  private String updatePlayer(Request request, Response response) {
    final String id = request.params(":id");
    response.type("nothing");
    final PlayerRecordV2 playerRecord;
    try {
      playerRecord = gson.fromJson(request.body(), PlayerRecordV2.class);
    } catch (JsonSyntaxException e) {
      response.status(422);
      return "";
    }
    if(!isValidateRecord(playerRecord, id)){
      response.status(422);
      return "";
    }
    playerRecord.setId(id);
    if(playerStorage.getPlayerByID(playerRecord.getPlayerID()) == null){
      response.header("Location", request.url());
      response.status(201);
    } else {
      response.status(204);
    }
    playerStorage.updatePlayerRecord(translator.forward(playerRecord));
    return "";
  }

  private boolean isValidateRecord(PlayerRecordV2 playerRecord, String id) {
    return id.equals(playerRecord.getPlayerID())
            && !isNullOrEmpty(playerRecord.getGroupName())
            && !isNullOrEmpty(playerRecord.getFirstName())
            && !isNullOrEmpty(playerRecord.getPositionAsString())
            && positionRegex.matcher(playerRecord.getPositionAsString()).matches()
            && playerRecord.getRegion() != null;
  }

  private PlayerRecordV2 getPlayer(Request request, Response response) {
    String params = request.params(":id");

    final PlayerRecordV2 playerByID = translator.back(playerStorage.getPlayerByID(params));

    if(playerByID == null){
      response.status(404);
      response.type("nothing");
      return null;
    }
    response.type(jsonContentType);
    return playerByID;
  }
}
