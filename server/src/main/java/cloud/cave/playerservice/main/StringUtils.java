package cloud.cave.playerservice.main;

public class StringUtils {
  public static boolean isNullOrEmpty( String target ) {
    return target == null || target.isEmpty();
  }
}
