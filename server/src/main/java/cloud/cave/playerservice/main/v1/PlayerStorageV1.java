package cloud.cave.playerservice.main.v1;

import java.util.List;

public interface PlayerStorageV1 {
    PlayerRecordV1 getPlayerByID(String playerID);

    void updatePlayerRecord(PlayerRecordV1 record);

    List<PlayerRecordV1> computeListOfPlayersAt(String positionString);

    void disconnect();
}
