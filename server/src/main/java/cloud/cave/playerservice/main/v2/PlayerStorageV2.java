package cloud.cave.playerservice.main.v2;

import java.util.List;

public interface PlayerStorageV2 {
    PlayerRecordV2 getPlayerByID(String playerID);

    UpdateStatus updatePlayerRecord(PlayerRecordV2 record);

    List<PlayerRecordV2> computeListOfPlayersAt(String positionString);

    void disconnect();
}
