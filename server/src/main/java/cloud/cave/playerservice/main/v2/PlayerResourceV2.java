package cloud.cave.playerservice.main.v2;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;
import spark.RouteGroup;

import java.util.List;
import java.util.regex.Pattern;

import static cloud.cave.playerservice.main.StringUtils.isNullOrEmpty;
import static spark.Spark.get;
import static spark.Spark.put;

public class PlayerResourceV2 implements RouteGroup {
  private final static Logger logger = LoggerFactory.getLogger(PlayerResourceV2.class);
  private final static Gson gson = new Gson();

  private final static Pattern positionRegex = Pattern.compile("\\(-?\\d+,-?\\d+,-?\\d+\\)");
  private final static String jsonContentType = "application/json";

  private final PlayerStorageV2 playerStorage;

  public PlayerResourceV2(PlayerStorageV2 playerStorage) {
    this.playerStorage = playerStorage;
  }

  @Override
  public void addRoutes() {
    get("", this::getPlayersAtPosition, this::jsonTransformer);
    get("/:id", this::getPlayer, this::jsonTransformer);
    put("/:id", this::updatePlayer);
  }

  private String jsonTransformer(Object o) {
    if(o == null) {
      return "";
    }
    return gson.toJson(o);
  }

  private List<PlayerRecordV2> getPlayersAtPosition(Request request, Response response) {
    final String position = request.queryParams("position");
    if(!positionRegex.matcher(position).matches()){
      response.status(400);
      return null;
    }
    response.type(jsonContentType);
    return playerStorage.computeListOfPlayersAt(position);
  }

  private String updatePlayer(Request request, Response response) {
    final String id = request.params(":id");
    response.type("nothing");
    final PlayerRecordV2 playerRecord;
    try {
      playerRecord = gson.fromJson(request.body(), PlayerRecordV2.class);
    } catch (JsonSyntaxException e) {
      response.status(422);
      return "";
    }
    if(!isValidateRecord(playerRecord, id)){
      response.status(422);
      return "";
    }
    playerRecord.setId(id);
    final UpdateStatus updateStatus = playerStorage.updatePlayerRecord(playerRecord);
    switch (updateStatus) {
      case CREATED:
        response.header("Location", request.url());
        response.status(201);
        break;
      case UPDATED:
        response.status(204);
        break;
    }
    playerStorage.updatePlayerRecord(playerRecord);
    return "";
  }

  private boolean isValidateRecord(PlayerRecordV2 playerRecord, String id) {
    return id.equals(playerRecord.getPlayerID())
            && !isNullOrEmpty(playerRecord.getGroupName())
            && !isNullOrEmpty(playerRecord.getFirstName())
            && !isNullOrEmpty(playerRecord.getLastName())
            && !isNullOrEmpty(playerRecord.getPositionAsString())
            && positionRegex.matcher(playerRecord.getPositionAsString()).matches()
            && playerRecord.getRegion() != null;
  }

  private PlayerRecordV2 getPlayer(Request request, Response response) {
    String params = request.params(":id");

    final PlayerRecordV2 playerByID = playerStorage.getPlayerByID(params);

    if(playerByID == null){
      response.status(404);
      response.type("nothing");
      return null;
    }
    response.type(jsonContentType);
    return playerByID;
  }
}
