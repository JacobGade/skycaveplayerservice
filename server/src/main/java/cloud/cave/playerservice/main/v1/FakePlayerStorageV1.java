package cloud.cave.playerservice.main.v1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FakePlayerStorageV1 implements PlayerStorageV1 {
  public FakePlayerStorageV1() {
    playersById = new HashMap<>(5);
  }

  Map<String, PlayerRecordV1> playersById;

  @Override
  public PlayerRecordV1 getPlayerByID(String playerID) {
    return playersById.get(playerID);
  }

  @Override
  public void updatePlayerRecord(PlayerRecordV1 record) {
    playersById.put(record.getPlayerID(), record);
  }

  @Override
  public List<PlayerRecordV1> computeListOfPlayersAt(String positionString) {
    List<PlayerRecordV1> theList = new ArrayList<>();
    for ( String id : playersById.keySet() ) {
      PlayerRecordV1 ps = playersById.get(id);
      if (ps.isInCave() && ps.getPositionAsString().equals(positionString)) {
        theList.add(ps);
      }
    }
    return theList;
  }

  @Override
  public void disconnect() {

  }

  @Override
  public String toString() {
    return "{ type : InMemoryPlayerStorage }";
  }
}
