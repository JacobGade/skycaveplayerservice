package cloud.cave.playerservice.main.v1;

import cloud.cave.playerservice.main.Region;
import cloud.cave.playerservice.main.Translator;
import cloud.cave.playerservice.main.v2.PlayerRecordV2;

public class PlayerTranslatorV1ToV2 implements Translator<PlayerRecordV1, PlayerRecordV2> {

  @Override
  public PlayerRecordV2 forward(PlayerRecordV1 input) {
    if(input == null){
      return null;
    }
    Region region;
    try {
      region = Region.valueOf(input.getRegion());
    } catch (IllegalArgumentException ignored){
      region = Region.UNKNOWN;
    }

    final String[] playerName = input.getPlayerName().split(" ", 2);
    final String firstName = playerName.length > 0 ? playerName[0] : null;
    final String lastName = playerName.length == 2 ? playerName[1] : null;
    return new PlayerRecordV2(input.getPlayerID(),
                              firstName,
                              lastName,
                              input.getGroupName(),
                              region,
                              input.getPositionAsString(),
                              input.getSessionId());
  }

  @Override
  public PlayerRecordV1 back(PlayerRecordV2 input) {
    if(input == null){
      return null;
    }
    final Region region = input.getRegion();

    String playerName = null;
    if(input.getFirstName() != null && input.getLastName() != null){
      playerName = input.getFirstName() + " "+ input.getLastName();
    } else if(input.getFirstName() != null){
      playerName = input.getFirstName();
    } else if(input.getLastName() != null){
      playerName = input.getLastName();
    }

    return new PlayerRecordV1(input.getPlayerID(),
                              playerName,
                              input.getGroupName(),
                              region != null ? region.name() : null,
                              input.getPositionAsString(),
                              input.getSessionId());
  }
}
