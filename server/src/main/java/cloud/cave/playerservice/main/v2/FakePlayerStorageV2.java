package cloud.cave.playerservice.main.v2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FakePlayerStorageV2 implements PlayerStorageV2 {
  public FakePlayerStorageV2() {
    playersById = new HashMap<>(5);
  }

  Map<String, PlayerRecordV2> playersById;

  @Override
  public PlayerRecordV2 getPlayerByID(String playerID) {
    return playersById.get(playerID);
  }

  @Override
  public UpdateStatus updatePlayerRecord(PlayerRecordV2 record) {
    final PlayerRecordV2 existing = getPlayerByID(record.getPlayerID());
    playersById.put(record.getPlayerID(), record);
    return existing == null ? UpdateStatus.CREATED
                            : UpdateStatus.UPDATED;
  }

  @Override
  public List<PlayerRecordV2> computeListOfPlayersAt(String positionString) {
    List<PlayerRecordV2> theList = new ArrayList<>();
    for ( String id : playersById.keySet() ) {
      PlayerRecordV2 ps = playersById.get(id);
      if (ps.isInCave() && ps.getPositionAsString().equals(positionString)) {
        theList.add(ps);
      }
    }
    return theList;
  }

  @Override
  public void disconnect() {

  }

  @Override
  public String toString() {
    return "{ type : InMemoryPlayerStorage }";
  }
}
