package cloud.cave.playerservice.main.v1;

import cloud.cave.playerservice.main.Region;
import cloud.cave.playerservice.main.Translator;
import cloud.cave.playerservice.main.v1.PlayerRecordV1;
import cloud.cave.playerservice.main.v2.PlayerRecordV2;

public class PlayerTranslatorV2ToV1 implements Translator<PlayerRecordV2, PlayerRecordV1> {

  @Override
  public PlayerRecordV2 back(PlayerRecordV1 input) {
    Region region;
    try {
      region = Region.valueOf(input.getRegion());
    } catch (IllegalArgumentException ignored){
      region = Region.UNKNOWN;
    }

    final String[] playerName = input.getPlayerName().split(" ", 2);
    final String firstName = playerName.length > 1 ? playerName[0] : null;
    final String lastName = playerName.length == 2 ? playerName[1] : null;
    return new PlayerRecordV2(input.getPlayerID(),
                              firstName,
                              lastName,
                              input.getGroupName(),
                              region,
                              input.getPositionAsString(),
                              input.getSessionId());
  }

  @Override
  public PlayerRecordV1 forward(PlayerRecordV2 input) {
    final Region region = input.getRegion();
    return new PlayerRecordV1(input.getPlayerID(),
                              input.getFirstName()+" "+input.getLastName(),
                              input.getGroupName(),
                              region != null ? region.name() : null,
                              input.getPositionAsString(),
                              input.getSessionId());
  }
}
