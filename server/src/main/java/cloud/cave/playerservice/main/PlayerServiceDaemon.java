package cloud.cave.playerservice.main;

import cloud.cave.playerservice.main.v1.*;
import cloud.cave.playerservice.main.v2.FakePlayerStorageV2;
import cloud.cave.playerservice.main.v2.MongoPlayerStorageV2;
import cloud.cave.playerservice.main.v2.PlayerResourceV2;
import cloud.cave.playerservice.main.v2.PlayerStorageV2;
import org.slf4j.*;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;
import spark.Request;
import spark.Response;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;

import static cloud.cave.playerservice.main.StringUtils.isNullOrEmpty;
import static spark.Spark.*;

@Command(name = "playerService", mixinStandardHelpOptions = true, version = "playerService" +PlayerServiceDaemon.version,
        description = "Starts a playerservice for skycave.")
public class PlayerServiceDaemon implements Callable<Void> {
  private final static Logger logger = LoggerFactory.getLogger(PlayerServiceDaemon.class);
  final static String version = "1.6";

  @CommandLine.ArgGroup(exclusive = true)
  StorageConf storageConf;

  static class StorageConf {
    @CommandLine.ArgGroup(exclusive = false)
    ReplicaSetConfiguration replicaSetConf;

    static class ReplicaSetConfiguration {
      @Option(names = {"-d", "--databasehosts"}, required = true,
              description = "Hosts in the replicaset.\nFormat: HostA:PortA;HostB:PortB")
      String hosts = null;
      @Option(names = {"-r", "--replicaset"}, required = true,
              description = "The replicaset name to use for mongo")
      String replicaSet = null;
    }

    @Option(names = {"-e", "--databasehost"}, description = "Hosts to use for single db setup.\nFormat: HostA:PortA", required = true)
    String host = null;
  }

  @Option(names = {"-o", "--supportOldVersion"}, description = "Whether the API with old version is disabled.")
  boolean supportOldVersion = false;

  public static void main(String[] args) {
    new CommandLine(new PlayerServiceDaemon()).execute(args);
  }

  private void handleV1WithoutSlash() {
    if(supportOldVersion) {
      get("/v1", (q, a) -> {
        a.redirect("v1/");
        return null;
      });
    }
    get("/v2", (q, a) -> {
      a.redirect("v2/");
      return null;
    });
  }

  private static void logIncomingRequests(Request q, Response a) {
    if (!q.url().contains("/health")) {
      logger.info(requestInfoToString(q));
    }
  }

  private static String requestInfoToString(Request request) {
    StringBuilder sb = new StringBuilder();
    sb.append(request.requestMethod());
    sb.append(" ").append(request.url());
    if(!isNullOrEmpty(request.queryString())) {
      sb.append("?").append(request.queryString());
    }
    sb.append(" ").append(request.body());
    return sb.toString();
  }

  private static StandardConnectionConfig getConnectionConfig(String hostPortPair) {
    if (!hostPortPair.contains(":")) {
      logger.error("Invalid host/port pair: " + hostPortPair);
      throw new RuntimeException("Invalid host/port pair: " + hostPortPair);
    }
    final String[] split = hostPortPair.split(":");
    final String host = split[0];
    final int port;
    try {
      port = Integer.parseInt(split[1]);
    } catch (NumberFormatException e) {
      logger.error("Invalid port provided " + split[1]);
      throw new RuntimeException("Invalid port provided " + split[1]);
    }
    return new StandardConnectionConfig(host, port);
  }

  @Override
  public Void call() {
    PlayerStorageV2 playerStorageV2;
    if(storageConf != null && storageConf.host != null){
      logger.info("Using mongo db storage at "+storageConf.host);
      playerStorageV2 = new MongoPlayerStorageV2(getConnectionConfig(storageConf.host));
    } else if (storageConf != null && storageConf.replicaSetConf != null){
      String replicaSet = storageConf.replicaSetConf.replicaSet;
      String confHosts = storageConf.replicaSetConf.hosts;
      logger.info("Using mongo db storage with replica set "+replicaSet+" and hosts "+confHosts);
      String[] hosts = confHosts.split(";");
      List<ConnectionConfig> dbServers = Arrays.stream(hosts)
                                               .map(PlayerServiceDaemon::getConnectionConfig)
                                               .collect(Collectors.toList());

      playerStorageV2 = new MongoPlayerStorageV2(replicaSet, dbServers);
    } else {
      logger.info("Using in memory storage");
      playerStorageV2 = new FakePlayerStorageV2();
    }

    // Serve open api documentation
    staticFiles.location("/public");
    handleV1WithoutSlash();
    notFound("");
    before("/*", PlayerServiceDaemon::logIncomingRequests);
    if(supportOldVersion){
      path("/v1/players", new PlayerResourceV1(playerStorageV2, new PlayerTranslatorV1ToV2()));
    }
    path("/v2/players", new PlayerResourceV2(playerStorageV2));
    path("/health", new HealthResource(version, playerStorageV2));
    return null;
  }
}
