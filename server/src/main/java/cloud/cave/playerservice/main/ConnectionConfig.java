package cloud.cave.playerservice.main;

public interface ConnectionConfig {
    String getHostName();

    int getPortNumber();
}
