package cloud.cave.playerservice.main;

import cloud.cave.playerservice.main.v1.PlayerStorageV1;
import cloud.cave.playerservice.main.v2.PlayerStorageV2;
import spark.RouteGroup;

import java.net.Inet4Address;
import java.net.NetworkInterface;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import static spark.Spark.get;
import static spark.Spark.post;

public class HealthResource implements RouteGroup {
    private final AtomicInteger statusCode = new AtomicInteger(200);
    private final String version;
    private final PlayerStorageV2 playerStorage;

    public HealthResource(String version, PlayerStorageV2 playerStorage) {
        this.version = version;
        this.playerStorage = playerStorage;
    }

    @Override
    public void addRoutes() {
        get("", (request, response) -> {
            response.status(statusCode.get());
            response.type("application/json");
            return "{\n" +
                    "\tstatus : RUNNING,\n" +
                    "\tversion : " + version + ",\n"+
                    "\taddresses : ["+ Collections.list(NetworkInterface.getNetworkInterfaces()).stream()
                                                 .flatMap(ni -> Collections.list(ni.getInetAddresses()).stream())
                                                   .filter(ia -> !ia.isLoopbackAddress() && ia instanceof Inet4Address)
                                                 .map(ia -> "\""+ia.getHostAddress()+"\"")
                                                 .collect(Collectors.joining(", "))+"],\n"+
                    "\tJVM : \""+ System.getProperty("java.vm.name")+" - "+ System.getProperty("java.vm.version")+"\",\n" +
                    "\tjavaVersion : \""+ System.getProperty("java.version")+"\",\n" +
                    "\tplayerStorage : "+ playerStorage+"\n"+
                    "}";
        });

        post("", (request, response) -> {
            statusCode.set(Integer.parseInt(request.body()));
            return "";
        });
    }
}
