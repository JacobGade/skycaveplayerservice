package cloud.cave.playerservice.main;

public class StandardConnectionConfig implements ConnectionConfig {

    private final String hostName;
    private final int portNumber;

    public StandardConnectionConfig(String hostName, int portNumber) {
        this.hostName = hostName;
        this.portNumber = portNumber;
    }

    @Override
    public String getHostName() {
        return hostName;
    }

    @Override
    public int getPortNumber() {
        return portNumber;
    }

    @Override
    public String toString() {
        return hostName + ":" + portNumber;
    }
}
