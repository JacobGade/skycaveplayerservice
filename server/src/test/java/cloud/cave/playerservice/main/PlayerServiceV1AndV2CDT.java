package cloud.cave.playerservice.main;

import cloud.cave.playerservice.main.v1.PlayerRecordV1;
import cloud.cave.playerservice.main.v2.PlayerRecordV2;
import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.http.entity.mime.MIME;
import org.junit.ClassRule;
import org.junit.Test;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.output.OutputFrame;

import javax.servlet.http.HttpServletResponse;
import java.util.function.Consumer;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class PlayerServiceV1AndV2CDT {
    private static final Integer SERVER_PORT = 4567;
    private static final String playerV1Endpoint = "http://localhost:%s/v1/players";
    private static final String playerV2Endpoint = "http://localhost:%s/v2/players";

    static {
        Unirest.setObjectMapper(new GsonObjectMapper());
    }

    @ClassRule
    @SuppressWarnings("rawtypes")
    public static GenericContainer playerservice =
            new GenericContainer("anonarelegion/playerservice")
                    .withExposedPorts(SERVER_PORT)
                    .withCommand("java","-jar","daemon.jar","-o")
                    .withLogConsumer((Consumer<OutputFrame>) of -> System.out.println(of.getUtf8String()));
    private final static String jsonContentType = "application/json";

    @Test
    public void shouldRetrieveV1CreatedPlayerFromV2() throws UnirestException {
        final String playerID = "1";
        final String playerName = "Bent Doe";
        final String groupName = "Whatever";
        final String region = Region.AARHUS.name();
        final String position = "(0,0,0)";
        final String sessionId = "SessionId";

        PlayerRecordV1 playerRecord = new PlayerRecordV1(playerID,
                                                         playerName,
                                                         groupName,
                                                         region,
                                                         position,
                                                         sessionId);
        putPlayerV1(playerID, playerRecord);

        final HttpResponse<PlayerRecordV2> getPlayerResponse = getPlayerV2(playerID);
        final PlayerRecordV2 body = getPlayerResponse.getBody();

        assertThat(body.getPlayerID(), equalTo(playerID));
        assertThat(body.getFirstName(), equalTo("Bent"));
        assertThat(body.getLastName(), equalTo("Doe"));
        assertThat(body.getGroupName(), equalTo(groupName));
        assertThat(body.getRegion(), equalTo(Region.AARHUS));
        assertThat(body.getPositionAsString(), equalTo(position));
        assertThat(body.getSessionId(), equalTo(sessionId));
    }

    @Test
    public void shouldRetrieveV2CreatedPlayerFromV1() throws UnirestException {
        final String playerID = "2";
        final String playerFirstName = "Bent";
        final String playerLastName = "Burg";
        final String groupName = "Whatever";
        final Region region = Region.AARHUS;
        final String position = "(0,0,0)";
        final String sessionId = "SessionId";

        PlayerRecordV2 playerRecord = new PlayerRecordV2(playerID,
                                                         playerFirstName,
                                                         playerLastName,
                                                         groupName,
                                                         region,
                                                         position,
                                                         sessionId);
        putPlayerV2(playerID, playerRecord);

        final HttpResponse<PlayerRecordV1> getPlayerResponse = getPlayerV1(playerID);
        final PlayerRecordV1 body = getPlayerResponse.getBody();

        assertThat(body.getPlayerID(), equalTo(playerID));
        assertThat(body.getPlayerName(), equalTo(playerFirstName + " " + playerLastName));
        assertThat(body.getGroupName(), equalTo(groupName));
        assertThat(body.getRegion(), equalTo(Region.AARHUS.name()));
        assertThat(body.getPositionAsString(), equalTo(position));
        assertThat(body.getSessionId(), equalTo(sessionId));
    }

    @Test
    public void createV1updateV2readV1() throws UnirestException {
        final String playerID = "3";
        final String playerName = "Bent Doe";
        final String groupName = "Whatever";
        final String region = Region.AARHUS.name();
        final String position = "(0,0,0)";
        final String sessionId = "SessionId";

        // Create player through V1 API
        PlayerRecordV1 playerRecord = new PlayerRecordV1(playerID,
                                                         playerName,
                                                         groupName,
                                                         region,
                                                         position,
                                                         sessionId);
        putPlayerV1(playerID, playerRecord);

        // Update player through V2 API
        final HttpResponse<PlayerRecordV2> getPlayerV2Response = getPlayerV2(playerID);
        PlayerRecordV2 playerV2 = getPlayerV2Response.getBody();
        playerV2 = new PlayerRecordV2(playerV2.getPlayerID(),
                                  "NewName",
                                  playerV2.getLastName(),
                                  playerV2.getGroupName(),
                                  Region.COPENHAGEN,
                                  playerV2.getPositionAsString(),
                                  playerV2.getSessionId());
        putPlayerV2(playerV2.getPlayerID(), playerV2);

        // Read and verify player through V1 API
        final HttpResponse<PlayerRecordV1> getPlayerV1Response = getPlayerV1(playerID);
        final PlayerRecordV1 playerV1 = getPlayerV1Response.getBody();

        assertThat(playerV1.getPlayerID(), equalTo(playerID));
        assertThat(playerV1.getPlayerName(), equalTo("NewName Doe"));
        assertThat(playerV1.getGroupName(), equalTo(groupName));
        assertThat(playerV1.getRegion(), equalTo(Region.COPENHAGEN.name()));
        assertThat(playerV1.getPositionAsString(), equalTo(position));
        assertThat(playerV1.getSessionId(), equalTo(sessionId));
    }

    @Test
    public void createV2updateV1readV2() throws UnirestException {
        final String playerID = "4";
        final String playerFirstName = "Bent";
        final String playerLastName = "Burg";
        final String groupName = "Whatever";
        final Region region = Region.AARHUS;
        final String position = "(0,0,0)";
        final String sessionId = "SessionId";

        //Create player through V2 API
        PlayerRecordV2 playerRecord = new PlayerRecordV2(playerID,
                                                         playerFirstName,
                                                         playerLastName,
                                                         groupName,
                                                         region,
                                                         position,
                                                         sessionId);
        putPlayerV2(playerID, playerRecord);

        // Update player through V1 API
        final HttpResponse<PlayerRecordV1> getPlayerV1Response = getPlayerV1(playerID);
        PlayerRecordV1 playerV1 = getPlayerV1Response.getBody();
        playerV1 = new PlayerRecordV1(playerID,
                                      "Bente Burg",
                                      groupName,
                                      Region.COPENHAGEN.name(),
                                      position,
                                      sessionId);
        putPlayerV1(playerID, playerV1);

        // Read and verify player through V2 API
        final HttpResponse<PlayerRecordV2> getPlayerV2Response = getPlayerV2(playerID);
        final PlayerRecordV2 playerV2 = getPlayerV2Response.getBody();

        assertThat(playerV2.getPlayerID(), equalTo(playerID));
        assertThat(playerV2.getFirstName(), equalTo("Bente"));
        assertThat(playerV2.getLastName(), equalTo(playerLastName));
        assertThat(playerV2.getGroupName(), equalTo(groupName));
        assertThat(playerV2.getRegion(), equalTo(Region.COPENHAGEN));
        assertThat(playerV2.getPositionAsString(), equalTo(position));
        assertThat(playerV2.getSessionId(), equalTo(sessionId));
    }

    private void assertFailedPut(PlayerRecordV2 invalidPositionPlayer) throws UnirestException {
        final HttpResponse<String> putPlayerResponse = putPlayerV2("1", invalidPositionPlayer);
        assertThat(putPlayerResponse.getStatus(), equalTo(422));
    }

    private HttpResponse<PlayerRecordV2> getPlayerV2(String id) throws UnirestException {
        return Unirest.get(getPlayerV2Url()+"/"+id).asObject(PlayerRecordV2.class);
    }

    private HttpResponse<String> putPlayerV2(String id, PlayerRecordV2 content) throws UnirestException {
        return Unirest.put(getPlayerV2Url()+"/"+id)
                      .body(content)
                      .asString();
    }

    private HttpResponse<PlayerRecordV1> getPlayerV1(String id) throws UnirestException {
        return Unirest.get(getPlayerV1Url()+"/"+id).asObject(PlayerRecordV1.class);
    }

    private HttpResponse<String> putPlayerV1(String id, PlayerRecordV1 content) throws UnirestException {
        return Unirest.put(getPlayerV1Url()+"/"+id)
                      .body(content)
                      .asString();
    }

    private String getPlayerV1Url() {
        return String.format(playerV1Endpoint, playerservice.getMappedPort(SERVER_PORT));
    }

    private String getPlayerV2Url() {
        return String.format(playerV2Endpoint, playerservice.getMappedPort(SERVER_PORT));
    }

    private static class GsonObjectMapper implements ObjectMapper {
        private final Gson gson = new Gson();

        public <T> T readValue(String s, Class<T> aClass) {
            try {
                return gson.fromJson(s, aClass);
            } catch(Exception e) {
                throw new RuntimeException(e);
            }
        }

        public String writeValue(Object o) {
            try {
                return gson.toJson(o);
            } catch(Exception e){
                throw new RuntimeException(e);
            }
        }
    }
}
