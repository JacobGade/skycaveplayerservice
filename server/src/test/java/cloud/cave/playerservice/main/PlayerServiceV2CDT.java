package cloud.cave.playerservice.main;

import cloud.cave.playerservice.main.v1.PlayerRecordV1;
import cloud.cave.playerservice.main.v2.PlayerRecordV2;
import com.google.gson.Gson;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.ObjectMapper;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.apache.http.entity.mime.MIME;
import org.junit.ClassRule;
import org.junit.Test;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.output.OutputFrame;

import javax.servlet.http.HttpServletResponse;
import java.util.function.Consumer;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class PlayerServiceV2CDT {
    private static final Integer SERVER_PORT = 4567;
    private static final String playerEndpoint = "http://localhost:%s/v2/players";

    static {
        Unirest.setObjectMapper(new GsonObjectMapper());
    }

    @ClassRule
    @SuppressWarnings("rawtypes")
    public static GenericContainer mongoDBcontainer =
            new GenericContainer("anonarelegion/playerservice")
                    .withExposedPorts(SERVER_PORT)
                    .withLogConsumer((Consumer<OutputFrame>) of -> System.out.println(of.getUtf8String()));
    private final static String jsonContentType = "application/json";

    @Test
    public void shouldRetrieveCreatedPlayer() throws UnirestException {
        final String playerID = "2";
        final String playerFirstName = "Bent";
        final String playerLastName = "Burg";
        final String groupName = "Whatever";
        final Region region = Region.AARHUS;
        final String position = "(0,0,0)";
        final String sessionId = "SessionId";

        PlayerRecordV2 playerRecord = new PlayerRecordV2(playerID,
                                                         playerFirstName,
                                                         playerLastName,
                                                         groupName,
                                                         region,
                                                         position,
                                                         sessionId);
        final HttpResponse<String> putPlayerResponse = putPlayer(playerID, playerRecord);
        assertThat(putPlayerResponse.getStatus(), equalTo(HttpServletResponse.SC_CREATED));
        assertThat(putPlayerResponse.getHeaders().getFirst("Location"), equalTo(getPlayerUrl()+"/"+playerID));
        assertThat(putPlayerResponse.getHeaders().getFirst(MIME.CONTENT_TYPE), equalTo("nothing"));

        final HttpResponse<PlayerRecordV2> getPlayerResponse = getPlayer(playerID);
        assertThat(getPlayerResponse.getStatus(), equalTo(HttpServletResponse.SC_OK));
        assertThat(getPlayerResponse.getHeaders().getFirst(MIME.CONTENT_TYPE), equalTo(jsonContentType));
        final PlayerRecordV2 body = getPlayerResponse.getBody();

        assertThat(body.getPlayerID(), equalTo(playerID));
        assertThat(body.getFirstName(), equalTo(playerFirstName));
        assertThat(body.getLastName(), equalTo(playerLastName));
        assertThat(body.getGroupName(), equalTo(groupName));
        assertThat(body.getRegion(), equalTo(region));
        assertThat(body.getPositionAsString(), equalTo(position));
        assertThat(body.getSessionId(), equalTo(sessionId));
    }

    @Test
    public void shouldFindPlayersByPosition() throws UnirestException {
        final String position = "(0,0,0)";
        PlayerRecordV2 playerRecord1Match = new PlayerRecordV2("1", "1", "1", "1", Region.COPENHAGEN, position, "1");
        PlayerRecordV2 playerRecord2Match = new PlayerRecordV2("2", "2", "2", "2", Region.ODENSE, position, "2");
        PlayerRecordV2 playerRecord3NoMatch = new PlayerRecordV2("3", "3", "3", "3", Region.AARHUS, "(0,0,1)", "3");
        putPlayer("1", playerRecord1Match);
        putPlayer("2", playerRecord2Match);
        putPlayer("3", playerRecord3NoMatch);

        final HttpResponse<PlayerRecordV2[]> playersAtResponse = getPlayersAt(position);

        assertThat(playersAtResponse.getStatus(), equalTo(HttpServletResponse.SC_OK));
        assertThat(playersAtResponse.getHeaders().getFirst(MIME.CONTENT_TYPE), equalTo(jsonContentType));
        assertThat(playersAtResponse.getBody(), arrayContaining(playerRecord1Match, playerRecord2Match));
    }

    @Test
    public void should400WhenFindPlayersByPositionIsCalledWithInvalidPosition() throws UnirestException {
        final String invalidPosition = "(0,0,0";
        final HttpResponse<PlayerRecordV2[]> playersAtResponse = getPlayersAt(invalidPosition);

        assertThat(playersAtResponse.getStatus(), equalTo(HttpServletResponse.SC_BAD_REQUEST));
    }

    @Test
    public void should404OnNonExistingPlayer() throws UnirestException {
        final HttpResponse<String> getPlayerResponse = getPlayerString("NonExisting");
        assertThat(getPlayerResponse.getStatus(), equalTo(HttpServletResponse.SC_NOT_FOUND));
        assertThat(getPlayerResponse.getHeaders().getFirst(MIME.CONTENT_TYPE), equalTo("nothing"));
        assertThat(getPlayerResponse.getBody(), emptyOrNullString());
    }

    @Test
    public void should422WhenPuttingInvalidPlayerPosition() throws UnirestException {
        PlayerRecordV2 invalidPositionPlayer = new PlayerRecordV2("1","1","1",
                                                                  "1",Region.AARHUS,
                                                                  "NotAValidPosition",
                                                                  "1");
        assertFailedPut(invalidPositionPlayer);
    }

    @Test
    public void should422WhenPuttingPlayerWithMissingData() throws UnirestException {
        // To make the structure of the tests more clear we have simplified the arguments.
        // They are playerId, playerName, groupName, region, positionAsString and sessionId
        assertFailedPut(new PlayerRecordV2(null, "1", "1", "1", Region.AARHUS, "(0,0,0)", "NotImportant"));
        assertFailedPut(new PlayerRecordV2("1", null, "1", "1", Region.AARHUS, "(0,0,0)", "NotImportant"));
        assertFailedPut(new PlayerRecordV2("1", "1", "1", null, Region.AARHUS, "(0,0,0)", "NotImportant"));
        assertFailedPut(new PlayerRecordV2("1", "1", "1", "1", null, "(0,0,0)", "NotImportant"));
        assertFailedPut(new PlayerRecordV2("1", "1", "1", "1", Region.AARHUS, null, "NotImportant"));
    }

    @Test
    public void shouldAllowPutWithoutSessionId() throws UnirestException {
        final PlayerRecordV2 playerRecord = new PlayerRecordV2("shouldAllowPutWithoutSessionId",
                                                               "1", "1", "1",
                                                               Region.AARHUS, "(0,0,0)",
                                                               null);
        final HttpResponse<String> putPlayerResponse = putPlayer("shouldAllowPutWithoutSessionId", playerRecord);
        assertThat(putPlayerResponse.getStatus(), equalTo(201));
    }

    @Test
    public void should204OnUpdateToExistingPlayer() throws UnirestException {
        final String id = "should204OnUpdateToExistingPlayer";
        final PlayerRecordV2 playerRecord = new PlayerRecordV2(id, "1", "1", "1", Region.AARHUS, "(0,0,0)", null);
        putPlayer(id, playerRecord);
        final PlayerRecordV2 playerRecord2 = new PlayerRecordV2(id, "1", "1", "1", Region.AARHUS, "(0,0,0)", null);
        final HttpResponse<String> putPlayerResponse = putPlayer(id, playerRecord2);
        assertThat(putPlayerResponse.getStatus(), equalTo(204));
    }


    private void assertFailedPut(PlayerRecordV2 invalidPositionPlayer) throws UnirestException {
        final HttpResponse<String> putPlayerResponse = putPlayer("1", invalidPositionPlayer);
        assertThat(putPlayerResponse.getStatus(), equalTo(422));
    }

    private HttpResponse<PlayerRecordV2[]> getPlayersAt(String position) throws UnirestException {
        return Unirest.get(getPlayerUrl()+"?position="+position).asObject(PlayerRecordV2[].class);
    }

    private HttpResponse<PlayerRecordV2> getPlayer(String id) throws UnirestException {
        return Unirest.get(getPlayerUrl()+"/"+id).asObject(PlayerRecordV2.class);
    }

    private HttpResponse<String> getPlayerString(String id) throws UnirestException {
        return Unirest.get(getPlayerUrl()+"/"+id).asString();
    }

    private HttpResponse<String> putPlayer(String id, PlayerRecordV2 content) throws UnirestException {
        return Unirest.put(getPlayerUrl()+"/"+id)
                      .body(content)
                      .asString();
    }

    private String getPlayerUrl() {
        return String.format(playerEndpoint, mongoDBcontainer.getMappedPort(SERVER_PORT));
    }

    private static class GsonObjectMapper implements ObjectMapper {
        private final Gson gson = new Gson();

        public <T> T readValue(String s, Class<T> aClass) {
            try {
                return gson.fromJson(s, aClass);
            } catch(Exception e) {
                throw new RuntimeException(e);
            }
        }

        public String writeValue(Object o) {
            try {
                return gson.toJson(o);
            } catch(Exception e){
                throw new RuntimeException(e);
            }
        }
    }
}
