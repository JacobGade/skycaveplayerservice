package cloud.cave.playerservice.main;

import cloud.cave.playerservice.main.v1.MongoPlayerStorageV1;
import cloud.cave.playerservice.main.v1.PlayerRecordV1;
import cloud.cave.playerservice.main.v1.PlayerStorageV1;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.testcontainers.containers.GenericContainer;

import java.util.List;

import static org.hamcrest.CoreMatchers.either;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class PlayerServiceIntegrationTest {
  private static final int SERVER_PORT = 27017;
  private static final String id1 = "id02";
  private static final String id2 = "id-203212";

  @ClassRule @SuppressWarnings("rawtypes")
  public static GenericContainer mongoDBcontainer =
          new GenericContainer("mongo:4.2")
                  .withExposedPorts(SERVER_PORT);

  private PlayerStorageV1 storage;

  private PlayerRecordV1 player1;
  private PlayerRecordV1 player2;

  @Before
  public void setup() {
    storage = new MongoPlayerStorageV1(new StandardConnectionConfig("localhost", mongoDBcontainer.getMappedPort(SERVER_PORT)));
    player1 = new PlayerRecordV1(id1, "Tutmosis", "grp01", "ODENSE", "(0,0,0)", null);
    player2 = new PlayerRecordV1(id2, "MrLongName", "grp02", "COPENHAGEN", "(0,0,0)", null);
  }

  @After
  public void teardown() {
    storage.disconnect();
  }

  @Test
  public void shouldUpdatePlayerAndPositionTables() {

    final Point3 p876 = new Point3(8, 7, 6);
    final Point3 p273 = new Point3(2, 7, 3);
    // Add player
    String sessionid = "session1";

    addPlayerRecordToStorageForSubscription(player1, "session1");

    // and move him to position 2,7,3
    updatePlayerPosition(storage, id1, p273.getPositionString());

    // Tutmosis is in the cave
    assertThat(storage.getPlayerByID(id1).getPlayerName(), is(player1.getPlayerName()));
    assertThat(storage.getPlayerByID(id1).isInCave(), is(true));

    // get all players at 2,7,3
    List<PlayerRecordV1> ll = storage.computeListOfPlayersAt(p273.getPositionString());

    assertThat( ll.size(), is(1));
    assertThat( ll.get(0).getPlayerID(), is(id1));

    // and verify none are at 8,7,6
    ll = storage.computeListOfPlayersAt(p876.getPositionString());
    assertThat( ll.size(), is(0));

    // Intro another player
    addPlayerRecordToStorageForSubscription(player2, "session2");

    // move player 2 to same 8,7,6
    updatePlayerPosition(storage, id2, p876.getPositionString());

    ll = storage.computeListOfPlayersAt(p876.getPositionString());
    assertThat( ll.size(), is(1));
    assertThat( ll.get(0).getPlayerID(), is(id2));

    // move player 1 there also
    updatePlayerPosition(storage, id1, p876.getPositionString());

    // and verify that computation is correct
    ll = storage.computeListOfPlayersAt(p876.getPositionString());
    assertThat( ll.size(), is(2));
    assertThat( ll.get(0).getPlayerID(), either(is(id1)).
                                                                or(is(id2)));
    assertThat( ll.get(1).getPlayerID(), either(is(id1)).
                                                                or(is(id2)));
  }

  private static void updatePlayerPosition(PlayerStorageV1 storage, String id12, String positionString) {
    PlayerRecordV1 pRecord = storage.getPlayerByID(id12);
    pRecord.setPositionAsString(positionString);
    storage.updatePlayerRecord(pRecord);
  }

  @Test
  public void shouldUpdatePlayerTables() {
    addPlayerRecordToStorageForSubscription(player1, "session1");
    addPlayerRecordToStorageForSubscription(player2, "session2");

    // end session for player one
    PlayerRecordV1 rec1 = storage.getPlayerByID(id1);
    rec1.setSessionId(null);
    storage.updatePlayerRecord(rec1);

    // and the right one is left
    PlayerRecordV1 p;
    p = storage.getPlayerByID(id1);
    assertThat( p.isInCave(), is(false));

    p = storage.getPlayerByID(id2);
    assertThat( p.isInCave(), is(true));
  }

  private void addPlayerRecordToStorageForSubscription(PlayerRecordV1 player, String session) {
    player.setSessionId(session);
    storage.updatePlayerRecord(player);
  }

  @Test
  public void shouldGetPlayerByID() {
    addPlayerRecordToStorageForSubscription(player1, "session1");
    addPlayerRecordToStorageForSubscription(player2, "session2");

    PlayerRecordV1 p;
    p = storage.getPlayerByID(id1);
    assertThat( p.getPlayerName(), is(player1.getPlayerName()));
    p = storage.getPlayerByID(id2);
    assertThat( p.getPlayerName(), is(player2.getPlayerName()));
  }
}