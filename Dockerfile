#Create builder docker
FROM henrikbaerbak/jdk8-gradle as builder

LABEL maintainer="jon.jacobsen@getinge.com,jacob.gade@getinge.com"

# Support UTF-8
ENV LANG C.UTF-8

# Set the working directory
WORKDIR /root/playerservice

# No need to get fancy we are throwing all of this away afterwards anyways.
COPY . .

# Build single jar file
RUN ./gradlew fatjar


#Create operations docker. Using smaller image as we no longer need extra dependencies
FROM openjdk:8-jre-alpine
LABEL maintainer="jon.jacobsen@getinge.com,jacob.gade@getinge.com"

# Set the working directory
WORKDIR /root/playerservice

# Copy the fat jar to the new docker
COPY --from=builder /root/playerservice/server/build/libs/daemon.jar ./

# Set default command that runs the server with http.cpf configuration
CMD ["java","-jar","daemon.jar"]